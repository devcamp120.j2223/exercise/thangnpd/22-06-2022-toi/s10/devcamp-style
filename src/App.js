import image from './assets/images/48.jpg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className='container'>
      <img src={image} className='img' />
      <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills</p>
      <p><span className='author'>Tammy Stevens</span> - Front End Developer</p>
    </div>
  );
}

export default App;
